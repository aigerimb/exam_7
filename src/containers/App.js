import React, {Component} from 'react';
import {v4 as uuid} from "uuid";
import './App.css';
import ItemBoard from '../components/ItemBoard/ItemBoard';
import burgerImage from "../images/hamburger.png";
import pizzaImage from "../images/pizza-slice.png";
import pancakeImage from "../images/pancake.png";
import coffeeImage from "../images/coffee-cup.png";
import teaImage from "../images/iced-tea.png";
import colaImage from "../images/soda-can.png";
import EmptyOrderBoard from '../components/EmptyOrderBoard/EmptyOrderBoard';
import OrderDetails from '../components/OrderDetails/OrderDetails';

class App extends Component {
  state = {
    items: [
      {name: "Pizza", price: 1000, image: pizzaImage, id: uuid(), amount: 1},
      {name: "Coffee", price: 300, image: coffeeImage, id: uuid(), amount: 1},
      {name: "Cheeseburger", price: 1200, image: burgerImage, id: uuid(), amount: 1},
      {name: "Tea", price: 100, image: teaImage, id: uuid(), amount: 1},
      {name: "Pancake", price: 500, image: pancakeImage, id: uuid(), amount: 1},
      {name: "Cola", price: 250, image: colaImage, id: uuid(), amount: 1},
    ],
    orders: [], 
    totalPrice: 0
  }

  addItem = (id) => {
    const items = [...this.state.items];
    const orders = [...this.state.orders];
    let totalPrice = this.state.totalPrice;
    const foundItem = items.find(item => item.id === id);
    const newOrder = {...foundItem};
    const sameOrder = orders.find(order => order.id === newOrder.id);
    if (sameOrder) {
      sameOrder.amount++;
      sameOrder.price += newOrder.price;
      totalPrice += newOrder.price;
    }
    else {
      orders.push(newOrder);
      totalPrice += newOrder.price;
    }
    this.setState({orders, totalPrice});
  }

  deleteOrder = (id) => {
    const items = [...this.state.items];
    const orders = [...this.state.orders];
    let totalPrice = this.state.totalPrice;
    const foundItem = items.find(item => item.id === id);
    const newOrder = {...foundItem};
    const index = orders.findIndex(order => order.id === id);
    const sameOrder = orders.find(order => order.id === newOrder.id);
    if (sameOrder.amount > 1) {
      sameOrder.amount--;
      sameOrder.price -= newOrder.price;
      totalPrice -= newOrder.price;
    }
    else {
      orders.splice(index, 1);
      totalPrice -= newOrder.price;
    }
    this.setState({orders, totalPrice});
  }
    
  render() {
    return (
      <div className="App">
        <div className="Board">
          <ItemBoard 
            items={this.state.items}
            addItem={this.addItem}
          />
          {
            this.state.orders.length === 0 ? <EmptyOrderBoard /> : 
            <OrderDetails 
              orders={this.state.orders}
              deleteOrder={this.deleteOrder}
              totalPrice={this.state.totalPrice}
            />
          }
        </div>
      </div>
    );
  }
}

export default App;
