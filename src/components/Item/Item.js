import React from "react";
import "./Item.css";

const Item = props => {
    return (
			<div className="itemWrapper" onClick={props.addItem}>
				<img src={props.itemImage} alt="item icon" className="itemImage" />
				<div className="itemDescription">
					<h2 className="itemTitle">{props.name}</h2>
					<p className="itemPrice">Price: <span className="priceNumber">{props.price}</span> kzt</p>
				</div>
			</div>
    )
}

export default Item;