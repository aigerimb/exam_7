import React from "react";
import "./EmptyOrderBoard.css";

const EmptyOrderBoard = () => {
    return (
			<div className="ordersBoard">
				<h2 className="ordersBoardTitle">Order Details:</h2>
				<div className="emptyOrdersWrapper">
					<h4 className="emptyOrderText">Order is empty!</h4>
					<h4 className="emptyOrderText">Please add some items!</h4>
				</div>
			</div>
    )
}

export default EmptyOrderBoard;