import React from "react";
import "./Order.css";
import remove from "../../images/remove.png";

const Order = props => {
	return (
		<div className="orderWrapper">
			<h4 className="orderName">{props.orderName}</h4>
			<span className="orderAmount">{props.orderAmount}</span>
			<span className="orderPrice">{props.orderPrice} kzt</span>
			<button className="deleteButton" onClick={props.deleteOrder}>
				<img src={remove} alt="remove icon" className="deleteImg" />
			</button>
		</div>
	)
}

export default Order;