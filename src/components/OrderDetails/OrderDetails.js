import React from "react";
import "./OrderDetails.css";
import Order from "../Order/Order";

const OrderDetails = props => {
    return (
			<div className="ordersBoard">
				<h2 className="ordersBoardTitle">Order Details:</h2>
				<div className="ordersWrapper">
					{
						props.orders.map(order => {
							return (
								<Order
									key={order.id+order.name+order.amount}
									orderName={order.name}
									orderAmount={`x ${order.amount}`}
									orderPrice={order.price}
									deleteOrder={() => props.deleteOrder(order.id)}
								/>
							)
						})
					}
					<h3 className="totalPrice">
						<span className="totalPriceText">Total price: </span>
						{props.totalPrice} KZT
					</h3>
				</div>
			</div>
    )
}

export default OrderDetails;