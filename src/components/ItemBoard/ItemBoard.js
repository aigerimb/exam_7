import React from "react";
import "./ItemBoard.css";
import Item from "../Item/Item";

const ItemBoard = props => {
    return (
			<div className="itemBoard">
				<h2 className="itemBoardTitle">Add items:</h2>
				<div className="items">
					{
					props.items.map(item => {
						return (
						<Item
							key={item.id}
							itemImage={item.image}
							name={item.name}
							price={item.price}
							addItem={() => props.addItem(item.id)}
						/>
						)
					})}
				</div>
			</div>
    )
}

export default ItemBoard;